export default class Article {
    constructor() {
        this._id = new Date().getTime().toString();
        this.category = '';
        this.title = '';
        this.publishedAt = new Date();
        this.urlToImage = '';
        this.description = '';
    }
}
