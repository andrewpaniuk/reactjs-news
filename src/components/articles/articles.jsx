import React, { Component } from 'react';
import ArticlesList from '../_widgets/articles-list/articles-list'

class Articles extends Component {

    state = {
        articles: {
            items: []
        }
    }

    componentWillMount() {
        const items = JSON.parse(localStorage.getItem('articles'))
            .reverse() || []
        let articles = {};
        articles.items = items
        this.setState({
            articles
        })
    }

    render() {
        console.log(this.state.articles)
        return (
            <section className='p-news'>
                <div className="container">
                    < ArticlesList
                        page='articles'
                        title='Articles'
                        select='category'
                        data={this.state.articles}
                    />
                </div>
            </section>
        );
    }
}


export default Articles;

