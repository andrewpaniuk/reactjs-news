import React, { Component } from 'react'

import FontAwesome from "react-fontawesome";
import { NavLink } from "react-router-dom";
import SideNavigation from './side-navigation/side-navigation';
import SelectWidget from '../_widgets/select/select.widget';

export default class Menu extends Component {

    sideBars = () => {
        return (
            <div className="burger-btn">
                <FontAwesome name="bars" onClick={this.props.onOpenSideNav} />
            </div>
        )
    }
    render() {
        return (
            <section className="top-nav">
                <nav className="navbar navbar-expand-lg py-0">
                    <div className="container">
                        <div className="collapse navbar-collapse" id="exCollapsingNavbar2">
                            <ul className="nav navbar-nav ">
                                <li className="nav-item"> <NavLink exact activeClassName="active" className="nav-link" to="/">Home <span className="sr-only">(current)</span></NavLink> </li>
                                <li className="nav-item"> <NavLink activeClassName="active" className="nav-link" to="/articles">My news</NavLink> </li>
                                <li className="nav-item"> <NavLink activeClassName="active" className="nav-link" to="/sources">Sources</NavLink> </li>
                                <li className="nav-item"> <NavLink activeClassName="active" className="nav-link" to="/news">Lastest news</NavLink> </li>
                                <li className="nav-item"> <NavLink activeClassName="active" className="nav-link" to="/add-article">Add an article</NavLink> </li>
                            </ul>
                            <div className="burger">
                                <SideNavigation
                                    {...this.props}
                                />
                                {this.sideBars()}
                            </div>
                        </div>
                    </div>
                </nav>
            </section>

        )
    }
}
