import React, { Component } from 'react';
import Socials from '../socials/socials';
import Logo from '../logo/logo';
import Menu from '../menu/menu';


class Header extends Component {

    render() {
        return (
            <div>
                <div id="Date"></div>
                <div className="small-top">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4 date-sec">

                            </div>
                            <div className="col-lg-12">
                                <Socials/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="top-head left">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 col-lg-12">
                                <Logo/>
                            </div>
                        </div>
                    </div>
                </div>
                <Menu {...this.props}/>
            </div >
        );
    }
}

export default Header;
