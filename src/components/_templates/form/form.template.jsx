import React, { Component } from 'react'

export default class FormTemplate extends Component {
    renderFields = () => {
        const formArray = [];

        for (let elementName in this.props.formData) {
            formArray.push({
                id: elementName,
                settings: this.props.formData[elementName]
            })
        }

        return formArray.map((item, i) => {
            return (
                <div key={i} className="form_element">
                    {this.renderTemplates(item)}
                </div>
            )
        })
    }

    showLabel = (show, label) => show ? <label>{label}</label> : null

    changeHandler = (event, id, blur) => {
        const newState = this.props.formData;
        newState[id].value = event.target.value;

        // if (blur) {
        let validData = this.validate(newState[id])
        newState[id].valid = validData[0];
        newState[id].validationMessage = validData[1];
        // }
        newState[id].touched = true;

        this.props.change(newState)
    }

    validate = (element) => {
        console.log(element)
        let error = [true, ''];

        if (element.validation.minLen) {
            const valid = element.value.length >= element.validation.minLen;
            const message = `${!valid ? 'Must be greater than ' + element.validation.minLen : ''}`
            error = !valid ? [valid, message] : error
        }

        if (element.validation.required) {
            const valid = element.value.trim() !== '';
            const message = `${!valid ? 'This field is required' : ''}`

            error = !valid ? [valid, message] : error
        }

        return error;
    }

    showValidation = (data) => {
        let errorMessage = null;

        if (data.validation && !data.valid) {
            errorMessage = (
                <div className="invalid-feedback">
                    {data.validationMessage}
                </div>
            )
        }
        return errorMessage;
    }

    renderTemplates = (data) => {
        let formTemplate = '';
        let values = data.settings;

        switch (values.element) {
            case ('input'):
                formTemplate = (
                    <div>
                        {this.showLabel(values.label, values.labelText)}
                        <input
                            className='form-control'
                            {...values.config}
                            onBlur={
                                (event) => this.changeHandler(event, data.id, true)
                            }
                            onChange={
                                (event) => this.changeHandler(event, data.id, false)
                            }
                        />
                        {this.showValidation(values)}
                    </div>
                )
                break;
            case ('textarea'):
                formTemplate = (
                    <div>
                        {this.showLabel(values.label, values.labelText)}
                        <textarea
                            className='form-control'
                            {...values.config}
                            onChange={
                                (event) => this.changeHandler(event, data.id)
                            }
                        />
                    </div>
                )
                break;
            case ('select'):
                formTemplate = (
                    <div>
                        {this.showLabel(values.label, values.labelText)}
                        <select
                            className='form-control'
                            name={values.config.name}
                            value={values.value}
                            onChange={
                                (event) => this.changeHandler(event, data.id)
                            }
                        >
                            {values.config.options.map((item, i) => (
                                <option key={i} value={item.value}>
                                    {item.name}
                                </option>
                            ))}

                        </select>
                    </div>
                )
                break;
            default:
                formTemplate = null
        }
        return formTemplate;
    }

    render() {
        return (
            <div>
                {this.renderFields()}
            </div >
        )
    }
}
