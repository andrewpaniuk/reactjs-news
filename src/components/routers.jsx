import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";
import BaseLayout from './base-layout/base-layout';
import Home from "./home/home";
import News from "./news/news";
import Articles from "./articles/articles";
import Sources from './sources/sources';
import AddArticle from './add-article/add-article';
import ArticleDetail from './article-detail/article-detail';

class Routers extends Component {
    render() {
        return (
            <BaseLayout>
                <Switch>
                    <Route path="/" component={Home} exact />
                    <Route path="/sources" component={Sources} />
                    <Route path="/news" component={News} />
                    <Route path="/add-article" component={AddArticle} />
                    <Route path="/articles/:id" component={ArticleDetail} />
                    <Route path="/articles" component={Articles} />
                    <Route render={() => {
                        return (
                            <div className="container">
                                <div className="row">
                                    <div className="col-12">
                                        <center style={{paddingTop: '60px'}}>
                                            <h2>Sorry. You have found dead link</h2>
                                            <h1>404</h1>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        )
                    }} />
                </Switch>
            </BaseLayout>
        );
    }
}

export default Routers;
