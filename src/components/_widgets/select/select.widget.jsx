import React, { Component } from 'react';
import { connect } from 'react-redux';
import categories from '../../../categories.json'

class SelectWidget extends Component {

    countries = [
        {
            value: 'ua',
            name: 'Ukraine'
        },
        {
            value: 'fr',
            name: 'France'
        },
        {
            value: 'pl',
            name: 'Poland',
            selected: true
        },
    ]

    state = {
        selectedValue: 'all'
    }

    changeValue = (e) => {
        const target = e.target;
        const value = target.value;
        if (this.props.select == 'category') {
            this.props.onChangeNewsCategory(value)
            return;
        }
        this.props.onChangeNewsCountry(value);
    }

    showOptions = () => {

        if (this.props.select == 'country') {
            return this.countries.map((country, i) => {
                return (
                    <option key={i} value={country.value}>{country.name}</option>
                )
            })
        } else {
            return categories.map((category, i) => {
                return (
                    <option key={i} value={category.value}>{category.name}</option>
                )
            })
        }
    }
    render() {
        return (
            <div className="countries">
                <div className="input-field">
                    <select value={(this.props.select == 'country') ? this.props.news_country : this.props.news_category} name={this.props.page} onChange={this.changeValue}>
                        {this.showOptions()}
                    </select>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    news_country: state.news_country,
    news_category: state.news_category
})

const mapDispatchToProps = (dispatch) => ({
    onChangeNewsCountry: (news_country) => {
        dispatch({
            type: 'CHANGE_NEWS_COUNTRY',
            payload: news_country
        })
    },
    onChangeNewsCategory: (news_category) => {
        dispatch({
            type: 'CHANGE_NEWS_CATEGORY',
            payload: news_category
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(SelectWidget)


