import React, { Component } from 'react'
import Typed from 'react-typed'



export default class Logo extends Component {
    render() {
        return (
            <div className='logo'>
                <h1>AP News<small> <Typed
                    strings={[
                        'Welcome to my webite AP NEWS',
                        'You can find the lastest news only here',
                        'We are glad to see you',
                        'Have fun and a good day']}
                    typeSpeed={40}
                    backSpeed={50}>
                </Typed></small></h1>

            </div>
        )
    }
}
