import React, { Component } from 'react'

export default class Socials extends Component {
    render() {
        return (
            <div className="social-icon">
                <a target="_blank" href="#" className=" fa fa-facebook"></a>
                <a target="_blank" href="#" className=" fa fa-github"></a>
                <a target="_blank" href="#" className=" fa fa-gitlab"></a>
                <a target="_blank" href="#" className=" fa fa-linkedin"></a>
            </div>
        )
    }
}
