import React, { Component } from 'react';
import SelectWidget from '../_widgets/select/select.widget';

class Footer extends Component {
    render() {
        return (

            <section className="action-sec">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="header-select">
                                Country news:
                                <SelectWidget select='country' />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Footer;
