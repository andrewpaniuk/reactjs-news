export const SOURCES_REQUEST = 'SOURCES_REQUEST'
export const SOURCES_HAS_ERRORED = 'SOURCES_HAS_ERRORED'
export const SOURCES_IS_LOADING = 'SOURCES_IS_LOADING'
export const SOURCES_FETCH_DATA_SUCCESS = 'SOURCES_FETCH_DATA_SUCCESS'


export const sourcesRequest = (url) => {
    return {
        type: SOURCES_REQUEST,
        url
    }
}

export const sourcesHasErrored = () => {
    return {
        type: SOURCES_HAS_ERRORED
    }
}

export const sourcesIsLoading = () => {
    return {
        type: SOURCES_IS_LOADING
    }
}

export const sourcesFetchDataSuccess = items => {
    return {
        type: SOURCES_FETCH_DATA_SUCCESS,
        items
    }
}

export const fetchSourcesData = async (url) => {
    try {
        const res = await fetch(url)
        const data = await res.json()
        return data
    } catch (error) {
        console.log(error)
    }
}

