export const NEWS_REQUEST = 'NEWS_REQUEST'
export const NEWS_HAS_ERRORED = 'NEWS_HAS_ERRORED'
export const NEWS_IS_LOADING = 'NEWS_IS_LOADING'
export const NEWS_FETCH_DATA_SUCCESS = 'NEWS_FETCH_DATA_SUCCESS'

export const newsRequest = (url) => {
    return {
        type: NEWS_REQUEST,
        url
    }
}
export const newsHasErrored = () => {
    return {
        type: NEWS_HAS_ERRORED
    }
}

export const newsIsLoading = () => {
    return {
        type: NEWS_IS_LOADING
    }
}

export const newsFetchDataSuccess = (items) => {
    return {
        type: NEWS_FETCH_DATA_SUCCESS,
        items
    }
}

export const fetchNewsData = async (url) => {
    try {
        const res = await fetch(url)
        const data = await res.json()
        return data
    } catch (error) {
        console.log(error)
    }
}
