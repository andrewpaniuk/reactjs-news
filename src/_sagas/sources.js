import { put, call } from 'redux-saga/effects'

import { sourcesFetchDataSuccess, sourcesHasErrored, sourcesIsLoading, fetchSourcesData } from '../_actions/sources'

export default function* getSourcesData(action) {
    try {
        yield put(sourcesIsLoading())
        const data = yield call(fetchSourcesData, action.url)
        yield put(sourcesFetchDataSuccess(data))
    } catch (error) {
        yield put(sourcesHasErrored())
    }
}
