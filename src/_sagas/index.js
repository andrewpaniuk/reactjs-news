import { takeLatest } from 'redux-saga/effects'

import { NEWS_REQUEST } from '../_actions/news'
import getNewsData from './news'

import { SOURCES_REQUEST } from '../_actions/sources'
import getSourcesData from './sources'

export default function* rootSagas() {
    yield takeLatest(SOURCES_REQUEST, getSourcesData)
    yield takeLatest(NEWS_REQUEST, getNewsData)
}
