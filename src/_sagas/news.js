import { put, call} from 'redux-saga/effects'

import { fetchNewsData, newsFetchDataSuccess, newsHasErrored, newsIsLoading } from '../_actions/news'

export default function* getNewsData(action) {
    try {
        yield put(newsIsLoading())
        const data = yield call(fetchNewsData, action.url)
        yield put(newsFetchDataSuccess(data))
    } catch (error) {
        yield put(newsHasErrored())
    }
}
