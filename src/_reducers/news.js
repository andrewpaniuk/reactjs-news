const initialState = {
    items: [],
    isLoading: false,
    hasErrored: false
}

export const news = (state = initialState, action) => {
    switch (action.type) {
        case 'NEWS_HAS_ERRORED':
            return {
                ...state,
                items: [],
                isLoading: false,
                hasErrored: true
            }
        case 'NEWS_IS_LOADING':
            return {
                ...state,
                isLoading: true,
                hasErrored: false
            }
        case 'NEWS_FETCH_DATA_SUCCESS':
            return {
                ...state,
                items: action.items.articles,
                isLoading: false
            }
        default:
            return state
    }
}
