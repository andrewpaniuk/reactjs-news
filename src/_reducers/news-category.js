const localCategory = localStorage.getItem('news_category')
const initialState = localCategory || 'all';

const news_category = (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_NEWS_CATEGORY':
            localStorage.setItem('news_category', action.payload);
            return action.payload;
        default:
            return state;
    }
}

export default news_category;
