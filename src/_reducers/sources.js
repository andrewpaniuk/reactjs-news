const initialState = {
    items: [],
    isLoading: false,
    hasErrored: false
}

export const sources = (state = initialState, action) => {
    switch (action.type) {
        case 'SOURCES_HAS_ERRORED':
            return {
                ...state,
                items: [],
                isLoading: false,
                hasErrored: true
            }
        case 'SOURCES_IS_LOADING':
            return {
                ...state,
                isLoading: true,
                hasErrored: false
            }
        case 'SOURCES_FETCH_DATA_SUCCESS':
            return {
                ...state,
                items: action.items.sources,
                isLoading: false
            }
        default:
            return state
    }
}
