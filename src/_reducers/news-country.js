const localLanguage = localStorage.getItem('news_country')
const initialState = localLanguage || 'ua';

export default (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_NEWS_COUNTRY':
            localStorage.setItem('news_country', action.payload);
            return action.payload;
        default:
            return state;
    }
}


