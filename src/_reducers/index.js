import { combineReducers } from 'redux';

import news_country from './news-country';
import news_category from './news-category';
import { news } from './news'
import { sources } from './sources'

const reducer = combineReducers({
    news_country,
    news_category,
    news,
    sources
});

export default reducer;
